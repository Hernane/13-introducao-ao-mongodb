const mongo = require('mongodb');
const { MongoClient } = mongo;
const url = "mongodb://localhost:27017/";

MongoClient.connect(url, async (err, client) => { //primeiro parâmetro : idades a serem alteradas , segundo parâmetro : nova idade a ser inserida
    let idadeOrig = await parseInt(process.argv[2]);
    let idadeNova = await parseInt(process.argv[3]);
    const myQuery = { age: idadeOrig };   //documentos a serem alterados
    const newValue = { $set: { age: idadeNova } };   //novo valor a ser atribuído
    client.db("Hernane").collection("users").updateMany(myQuery,newValue,(err,item)=>{
        console.log(item.result.nModified + " documents atualizados");
        client.close();
    });
});