const mongo = require('mongodb');
const { MongoClient } = mongo;
const url = "mongodb://localhost:27017/";

MongoClient.connect(url, async (err, client) => {  //conecta ao servidor
    if (err) throw err;
    let pessoa = await process.argv[2];
    // client.db("Hernane").collection("users").findOne({ name: pessoa }, (err, item) => {  //encontra 1
    //     console.log(item);
    //     client.close();      
    // });
    client.db("Hernane").collection("users").find({}).toArray((err,item)=>{     //encontra vários
        console.log(item);   
        client.close();
    })
});
