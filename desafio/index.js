const mongo = require('mongodb');
const { MongoClient } = mongo;
const url = "mongodb://localhost:27017/";

MongoClient.connect(url, (err, client) => {   //conecta ao servidor
    if (err) throw err;
    const dbo = client.db("Hernane");   //nome do db
    const alunos = [       //lista de documentos
        { name: "hernane", age: 19, curso: "gti", semestre: "3 semestre", telefone: "99999-9999" },
        { name: "jessica", age: 28, curso: "ads", semestre: "2 semestre", telefone: "92222-2222" },
        { name: "renato", age: 20, curso: "gf", semestre: "4 semestre", telefone: "933333-3333" },
        { name: "galo", age: 19, curso: "gti", semestre: "3 semestre", telefone: "99999-9999" },
        { name: "leandro", age: 20, curso: "gti", semestre: "3 semestre", telefone: "99999-9999" },
        { name: "gustavo", age: 20., curso: "gti", semestre: "3 semestre", telefone: "99999-9999" },
        { name: "gabriel", age: 20, curso: "gti", semestre: "3 semestre", telefone: "99999-9999" },
        { name: "hernane", age: 12, curso: "gti", semestre: "3 semestre", telefone: "99999-9999" },
        { name: "hernane", age: 13, curso: "gti", semestre: "3 semestre", telefone: "99999-9999" },
        { name: "hernane", age: 14, curso: "gti", semestre: "3 semestre", telefone: "99999-9999" }
    ];
    dbo.createCollection("users", (err, res) => {   //cria a collection de nome users
        if (err) throw err;
        console.log(`coleção criada`);
    });
    dbo.collection("users").insertMany(alunos, (err, res) => {   //insere vários documentos na coleção users
        if (err) throw err;
        console.log(`foram inseridos ${res.insertedCount} documentos na coleção`);
        client.close(); //encerra 
    });
});
