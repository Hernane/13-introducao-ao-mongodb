const mongo = require('mongodb');
const {MongoClient} = mongo;
const url = "mongodb://localhost:27017/";

MongoClient.connect(url, async (err,client)=>{
    let pessoa = await process.argv[2];
    client.db("Hernane").collection("users").deleteOne({name: pessoa},(err,item)=>{  //deleta 1 documento
        if (err) throw err;
        console.log(`${item.result.n} documento foi deletado`);
        client.close();
    });
    // client.db("Hernane").collection("users").deleteMany({name: pessoa },(err,item)=>{  //deleta vários documento
    //     if (err) throw err;
    //     console.log(`${item.result.n} documentos foram deletados`);
    //     client.close();
    // });
});